package com.koml12.tutorthingbackend.gson;

public class DummyClass {
    String field1;
    String field2;

    DummyClass(String field1, String field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DummyClass) {
            DummyClass dummyClass = (DummyClass) obj;
            return this.field1.equals(dummyClass.field1) && this.field2.equals(dummyClass.field2);
        }
        return false;
    }
}
