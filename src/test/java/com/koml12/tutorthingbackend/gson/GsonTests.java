package com.koml12.tutorthingbackend.gson;

import com.google.gson.Gson;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

@RunWith(JUnit4.class)
public class GsonTests extends TestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testCanSerializeDummyClass() {
        DummyClass dummyClass = new DummyClass("field1", "field2");
        Gson gson = new Gson();
        String json = gson.toJson(dummyClass);
        String expected = "{\"field1\":\"field1\",\"field2\":\"field2\"}";
        assertEquals(expected, json);
    }

    @Test
    public void testCanGoFromJsonToDummyClass() {
        String json = "{\"field1\":\"field1\",\"field2\":\"field2\"}";
        DummyClass expected = new DummyClass("field1", "field2");
        Gson gson = new Gson();
        DummyClass actual = gson.fromJson(json, DummyClass.class);
        assertEquals(expected, actual);
    }

    @Test
    public void testCanSerializeListClass() {
        ArrayList<Integer> items = new ArrayList<>();
        items.add(0);
        items.add(1);
        items.add(2);
        ListClass listClass = new ListClass(5, items);
        Gson gson = new Gson();
        String actual = gson.toJson(listClass);
        assertEquals("", actual);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
