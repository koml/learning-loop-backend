package com.koml12.tutorthingbackend.gson;

import java.util.ArrayList;

public class ListClass {
    int code;
    ArrayList<Integer> items;

    public ListClass(int code, ArrayList<Integer> items) {
        this.code = code;
        this.items = items;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ListClass) {
            ListClass listClass = (ListClass) obj;
            return this.code == listClass.code && this.items.equals(listClass.items);
        }
        return false;
    }
}
