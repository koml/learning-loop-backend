package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Lessons are part of Sections. They contain more detailed information about a specific part of a Section.
 * Lessons are most similar to the "lessons" in a textbook chapter.
 */
@Entity
@Table(name = "lessons")
public class Lesson implements ChangeableModel<Lesson> {

    @Id
    @GeneratedValue(generator = "lesson_generator")
    @SequenceGenerator(
            name = "lesson_generator",
            sequenceName = "lesson_sequence"
    )
    private Long id;

    @Column(name = "lesson_title")
    private String title;

    @Column(name = "lesson_shortTitle")
    private String shortTitle;

    @Column(name = "lesson_code")
    private Integer code;

    @Column(name = "lesson_introUrl")
    private String introUrl;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "section_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Section section;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "sublesson_id")
    @OrderBy("code ASC ")
    private Set<Sublesson> sublessons;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "quizquestion_id")
    private Set<QuizQuestion> quizQuestions;

    @Override
    public void replaceFields(Lesson newModel) {
        if (newModel.id != null) {
            this.setId(newModel.getId());
        }
        this.setTitle(newModel.getTitle());
        this.setCode(newModel.getCode());
        this.setIntroUrl(newModel.getIntroUrl());
        this.setSection(newModel.getSection());
        this.setShortTitle(newModel.getShortTitle());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getIntroUrl() {
        return introUrl;
    }

    public void setIntroUrl(String introUrl) {
        this.introUrl = introUrl;
    }


    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public Set<Sublesson> getSublessons() {
        return sublessons;
    }

    public void setSublessons(Set<Sublesson> sublessons) {
        this.sublessons = sublessons;
    }

    public Set<QuizQuestion> getQuizQuestions() {
        return quizQuestions;
    }

    public void setQuizQuestions(Set<QuizQuestion> quizQuestions) {
        this.quizQuestions = quizQuestions;
    }
}
