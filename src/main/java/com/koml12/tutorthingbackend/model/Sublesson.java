package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Sublessons are part of Lessons. They contain more focused examples of material within a lesson.
 * Each Sublesson can be likened to a worked example in a textbook lesson.
 * Sublessons are simply containers for topics. They hold various VideoQuestionPairs, each focusing on a different
 * aspect of the topic.
 */
@Entity
@Table(name = "sublessons")
public class Sublesson implements ChangeableModel<Sublesson> {

    @Id
    @GeneratedValue(generator = "sublesson_generator")
    @SequenceGenerator(
            name = "sublesson_generator",
            sequenceName = "sublesson_sequence"
    )
    private Long id;

    @Column(name = "sublesson_title", columnDefinition = "text")
    private String title;

    @Column(name = "sublesson_shorttitle", columnDefinition = "text")
    private String shortTitle;

    @Column(name = "sublesson_code")
    private Integer code = 1;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "lesson_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Lesson lesson;

    @OneToMany(fetch =FetchType.LAZY)
    @JoinColumn(name = "videoquestionpair_id")
    private Set<VideoQuestionPair> videoQuestionPairs;

    @Override
    public void replaceFields(Sublesson newSublesson) {
        if (newSublesson.getId() != null) {
            this.setId(newSublesson.getId());
        }
        this.setTitle(newSublesson.getTitle());
        this.setShortTitle(newSublesson.getShortTitle());
        this.setLesson(newSublesson.getLesson());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Set<VideoQuestionPair> getVideoQuestionPairs() {
        return videoQuestionPairs;
    }

    public void setVideoQuestionPairs(Set<VideoQuestionPair> videoQuestionPairs) {
        this.videoQuestionPairs = videoQuestionPairs;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
