package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.annotation.Order;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "videoquestionpairs")
public class VideoQuestionPair implements ChangeableModel<VideoQuestionPair> {

    @Id
    @GeneratedValue(generator = "videoquestionpair_generator")
    @SequenceGenerator(
            name = "videoquestionpair_generator",
            sequenceName = "videoquestionpair_sequence"
    )
    private Long id;

    @Column(name = "videoquestionpair_videourl", columnDefinition = "text")
    private String videoUrl;

    @Column(name = "videoquestionpair_questiontext", columnDefinition = "text")
    private String questionText;

    @Column(name = "videoquestionpair_correctchoice", columnDefinition = "text")
    private String correctChoice;

    @Column(name = "videoquestionpair_errortype")
    private Integer errorType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sublesson_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Sublesson sublesson;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "videoquestionchoice_id")
    @OrderBy("errorType ASC")
    private Set<VideoQuestionChoice> videoQuestionChoices;

    @Override
    public void replaceFields(VideoQuestionPair newVideoQuestionPair) {
        if (newVideoQuestionPair.getId() != null) {
            this.setId(newVideoQuestionPair.getId());
        }
        this.setVideoUrl(newVideoQuestionPair.getVideoUrl());
        this.setQuestionText(newVideoQuestionPair.getQuestionText());
        this.setCorrectChoice(newVideoQuestionPair.getCorrectChoice());
        this.setSublesson(newVideoQuestionPair.getSublesson());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCorrectChoice() {
        return correctChoice;
    }

    public void setCorrectChoice(String correctChoice) {
        this.correctChoice = correctChoice;
    }

    public Sublesson getSublesson() {
        return sublesson;
    }

    public void setSublesson(Sublesson sublesson) {
        this.sublesson = sublesson;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Set<VideoQuestionChoice> getVideoQuestionChoices() {
        return videoQuestionChoices;
    }

    public void setVideoQuestionChoices(Set<VideoQuestionChoice> videoQuestionChoices) {
        this.videoQuestionChoices = videoQuestionChoices;
    }

    public Integer getErrorType() {
        return errorType;
    }

    public void setErrorType(Integer errorType) {
        this.errorType = errorType;
    }
}
