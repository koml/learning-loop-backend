package com.koml12.tutorthingbackend.model;

public interface ChangeableModel<T> {
    void replaceFields(T newModel);
}
