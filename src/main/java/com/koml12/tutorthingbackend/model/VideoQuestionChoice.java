package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Represents an answer choice of a VideoQuestionPair. Each Pair can have multiple Choices.
 */
@Entity
@Table(name = "videoquestionchoices")
public class VideoQuestionChoice implements ChangeableModel<VideoQuestionChoice> {

    @Id
    @GeneratedValue(generator = "sublesson_generator")
    @SequenceGenerator(
            name = "sublesson_generator",
            sequenceName = "sublesson_sequence"
    )
    private Long id;

    @Column(name = "videoquestionchoice_letter", columnDefinition = "text")
    private String letter;

    @Column(name = "videoquestionchoice_choicetext", columnDefinition = "text")
    private String choiceText;

    @Column(name = "videoquestionchoice_errortype")
    private Integer errorType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "videoquestionpair_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private VideoQuestionPair videoQuestionPair;

    @Override
    public void replaceFields(VideoQuestionChoice newVideoQuestionChoice) {
        if (newVideoQuestionChoice.getId() != null) {
            this.setId(newVideoQuestionChoice.getId());
        }
        this.setLetter(newVideoQuestionChoice.getLetter());
        this.setChoiceText(newVideoQuestionChoice.getChoiceText());
        this.setErrorType(newVideoQuestionChoice.getErrorType());
        this.setVideoQuestionPair(newVideoQuestionChoice.getVideoQuestionPair());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getChoiceText() {
        return choiceText;
    }

    public void setChoiceText(String choiceText) {
        this.choiceText = choiceText;
    }

    public Integer getErrorType() {
        return errorType;
    }

    public void setErrorType(Integer errorType) {
        this.errorType = errorType;
    }

    public VideoQuestionPair getVideoQuestionPair() {
        return videoQuestionPair;
    }

    public void setVideoQuestionPair(VideoQuestionPair videoQuestionPair) {
        this.videoQuestionPair = videoQuestionPair;
    }
}
