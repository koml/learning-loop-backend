package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Sections are part of Courses. They provide information about a particular topic of a Course.
 * Sections can be likened to "chapters" in a textbook.
 */
@Entity
@Table(name = "sections")
public class Section implements ChangeableModel<Section> {

    @Id
    @GeneratedValue(generator = "section_generator")
    @SequenceGenerator(
            name = "course_generator",
            sequenceName = "section_sequence"
    )
    private Long id;

    @Column(name = "section_title")
    private String title;

    @Column(name = "section_shortTitle")
    private String shortTitle;

    @Column(name = "section_code")
    private Integer code;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "course_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Course course;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "lesson_id")
    private Set<Lesson> lessons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public void replaceFields(Section newSection) {
        if (newSection.getId() != null) {
            this.setId(newSection.getId());
        }
        this.setCode(newSection.getCode());
        this.setTitle(newSection.getTitle());
        this.setCourse(newSection.getCourse());
        this.setShortTitle(newSection.getShortTitle());
    }


    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }
}
