package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Represents a quiz question to be given at the end of a lesson.
 * These questions do not do any alternate video handling like the VideoQuestionPair objects do.
 * They instead just provide one explanation video url if the student gets the question wrong.
 * Other than that, they operate similar to the VideoQuestionPair model.
 */
@Entity
@Table(name = "quizquestions")
public class QuizQuestion implements ChangeableModel<QuizQuestion> {

    @Id
    @GeneratedValue(generator = "quizquestion_generator")
    @SequenceGenerator(
            name = "quizquestion_generator",
            sequenceName = "quizquestion_sequence"
    )
    private Long id;

    @Column(name = "quizquestion_questiontext", columnDefinition = "text")
    private String questionText;

    @Column(name = "quizquestion_correctchoice", columnDefinition = "text")
    private String correctChoice;

    @Column(name = "quizquestion_explanationurl", columnDefinition = "text")
    private String explanationUrl;

    @Column(name = "quizquestion_questionnumber")
    private Integer questionNumber;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "lesson_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Lesson lesson;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "quizchoice_id")
    private Set<QuizChoice> quizChoices;

    @Override
    public void replaceFields(QuizQuestion newQuizQuestion) {
        if (newQuizQuestion.getId() != null) {
            this.setId(newQuizQuestion.getId());
        }
        this.setQuestionText(newQuizQuestion.getQuestionText());
        this.setCorrectChoice(newQuizQuestion.getCorrectChoice());
        this.setExplanationUrl(newQuizQuestion.getExplanationUrl());
        this.setLesson(newQuizQuestion.getLesson());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCorrectChoice() {
        return correctChoice;
    }

    public void setCorrectChoice(String correctChoice) {
        this.correctChoice = correctChoice;
    }

    public String getExplanationUrl() {
        return explanationUrl;
    }

    public void setExplanationUrl(String explanationUrl) {
        this.explanationUrl = explanationUrl;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Set<QuizChoice> getQuizChoices() {
        return quizChoices;
    }

    public void setQuizChoices(Set<QuizChoice> quizChoices) {
        this.quizChoices = quizChoices;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
    }
}
