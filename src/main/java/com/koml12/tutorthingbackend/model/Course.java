package com.koml12.tutorthingbackend.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Courses contain a complete knowledge set for a particular topic (i.e. Algebra or Geometry)
 * Each course can be likened to a "textbook" of knowledge for its topic.
 */
@Entity
@Table(name = "courses")
public class Course implements ChangeableModel<Course> {
    @Id
    @GeneratedValue(generator = "course_generator")
    @SequenceGenerator(
            name = "course_generator",
            sequenceName = "course_sequence"
    )
    private Long id;

    @Column(name = "course_title")
    private String title;

    @Column(name = "course_shortTitle")
    private String shortTitle;

    @Column(name = "course_code")
    private Integer code;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "section_id")
    private Set<Section> sections;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public void replaceFields(Course newCourse) {
        if (newCourse.getId() != null) {
            this.setId(newCourse.getId());
        }
        this.setTitle(newCourse.getTitle());
        this.setCode(newCourse.getCode());
        this.setShortTitle(newCourse.getShortTitle());
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public Set<Section> getSections() {
        return sections;
    }

    public void setSections(Set<Section> sections) {
        this.sections = sections;
    }
}
