package com.koml12.tutorthingbackend.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Students are the main users of our product. They can log in and register for courses.
 */
@Entity
@Table(name = "students")
public class Student implements ChangeableModel<Student> {

    @Id
    @GeneratedValue(generator = "student_generator")
    @SequenceGenerator(
            name = "student_generator",
            sequenceName = "student_sequence"
    )
    private Long id;

    @Column(name = "student_username", nullable = false)
    private String username;

    @Column(name = "student_password", nullable = false)
    private String password;

    @Column(name = "student_email", nullable = false)
    private String email;

    @Column(name = "student_firstname", nullable = false)
    private String firstName;

    @Column(name = "student_lastname")
    private String lastName;

    @Column(name = "student_birthday")
    private Date birthday;

    @Column(name = "student_points")
    private int points = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public void replaceFields(Student newStudent) {
        if (newStudent.getId() != null) {
            this.setId(newStudent.getId());
        }
        this.setUsername(newStudent.getUsername());
        this.setPassword(newStudent.getPassword());
        this.setEmail(newStudent.getEmail());
        this.setFirstName(newStudent.getFirstName());
        this.setLastName(newStudent.getLastName());
        this.setBirthday(newStudent.getBirthday());
        this.setPoints(newStudent.getPoints());
    }
}
