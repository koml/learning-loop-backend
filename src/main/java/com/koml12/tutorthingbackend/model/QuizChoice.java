package com.koml12.tutorthingbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "quizchoices")
public class QuizChoice implements ChangeableModel<QuizChoice> {

    @Id
    @GeneratedValue(generator = "quizchoice_generator")
    @SequenceGenerator(
            name = "quizchoice_generator",
            sequenceName = "quizchoice_sequence"
    )
    private Long id;

    @Column(name = "quizchoice_letter", columnDefinition = "text")
    private String letter;

    @Column(name = "quizchoice_choicetext", columnDefinition = "text")
    private String choiceText;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "quizquestion_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private QuizQuestion quizQuestion;

    @Override
    public void replaceFields(QuizChoice newModel) {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getChoiceText() {
        return choiceText;
    }

    public void setChoiceText(String choiceText) {
        this.choiceText = choiceText;
    }

    public QuizQuestion getQuizQuestion() {
        return quizQuestion;
    }

    public void setQuizQuestion(QuizQuestion quizQuestion) {
        this.quizQuestion = quizQuestion;
    }
}
