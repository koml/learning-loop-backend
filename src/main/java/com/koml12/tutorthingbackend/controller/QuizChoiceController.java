package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.QuizChoice;
import com.koml12.tutorthingbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class QuizChoiceController {

    @Autowired
    private QuizChoiceRepository quizChoiceRepository;

    @Autowired
    QuizQuestionRepository quizQuestionRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;


    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/quizquestions/{quizQuestionNumber}/choices/")
    public List<QuizChoice> getQuizChoicesForQuizQuestionFromCourseAndSectionAndLesson(@PathVariable String courseName,
                                                                                       @PathVariable String sectionName,
                                                                                       @PathVariable String lessonName,
                                                                                       @PathVariable Integer quizQuestionNumber) {
        return quizChoiceRepository.findByQuizQuestion(
                quizQuestionRepository.findByQuestionNumberAndLesson(
                        quizQuestionNumber,
                        lessonRepository.findByShortTitleAndSection(
                                lessonName,
                                sectionRepository.findByShortTitleAndCourse(
                                        sectionName,
                                        courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                                ).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        );
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/quizquestions/{quizQuestionNumber}/choices/")
    public QuizChoice addQuizChoiceForQuizQuestionFromCourseAndSectionAndLesson(@PathVariable String courseName,
                                                                                @PathVariable String sectionName,
                                                                                @PathVariable String lessonName,
                                                                                @PathVariable Integer quizQuestionNumber,
                                                                                @Valid @RequestBody QuizChoice quizChoice) {
        return quizQuestionRepository.findByQuestionNumberAndLesson(
                quizQuestionNumber,
                lessonRepository.findByShortTitleAndSection(
                        lessonName,
                        sectionRepository.findByShortTitleAndCourse(
                                sectionName,
                                courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        ).map(quizQuestion -> {
            quizChoice.setQuizQuestion(quizQuestion);
            Set<QuizChoice> existingQuizChoices = quizQuestion.getQuizChoices();
            existingQuizChoices.add(quizChoice);
            QuizChoice savedQuizChoice = quizChoiceRepository.save(quizChoice);
            quizQuestionRepository.save(quizQuestion);
            return savedQuizChoice;
        }).orElseThrow(ResourceNotFoundException::new);
    }
}
