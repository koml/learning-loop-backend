package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.Course;
import com.koml12.tutorthingbackend.model.Section;
import com.koml12.tutorthingbackend.repository.CourseRepository;
import com.koml12.tutorthingbackend.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class SectionController {

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections")
    public List<Section> getSectionsForCourse(@PathVariable String courseName) {
        return sectionRepository.findByCourseId(
                courseRepository.findByShortTitle(courseName)
                .map(Course::getId).orElseThrow(() -> new ResourceNotFoundException("No course found with name " + courseName))
        );
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections")
    public Section addSectionToCourse(@PathVariable String courseName, @Valid @RequestBody Section section) {
        return courseRepository.findByShortTitle(courseName)
                .map(course -> {
                    section.setCourse(course);
                    Set<Section> sections = course.getSections();
                    sections.add(section);
                    course.setSections(sections);
                    Section savedSection = sectionRepository.save(section);
                    courseRepository.save(course);
                    return savedSection;
                })
                .orElseThrow(() -> new ResourceNotFoundException("No course found with name " + courseName));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}")
    public Section getSectionByShortTitle(@PathVariable String courseName, @PathVariable String sectionName) {
        return sectionRepository.findByShortTitleAndCourse(sectionName,
                courseRepository.findByShortTitle(courseName)
                        .orElseThrow(() -> new ResourceNotFoundException("No course with name: " + courseName))
        ).orElseThrow(() -> new ResourceNotFoundException("No section with name: " + sectionName + " in course: " + courseName));
    }
}
