package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.Lesson;
import com.koml12.tutorthingbackend.model.Sublesson;
import com.koml12.tutorthingbackend.repository.CourseRepository;
import com.koml12.tutorthingbackend.repository.LessonRepository;
import com.koml12.tutorthingbackend.repository.SectionRepository;
import com.koml12.tutorthingbackend.repository.SublessonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class SublessonController {
    @Autowired
    private SublessonRepository sublessonRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;

    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons")
    public List<Sublesson> getSublessonsForCourseAndSectionAndLesson(@PathVariable String courseName,
                                                                     @PathVariable String sectionName,
                                                                     @PathVariable String lessonName) {
        return sublessonRepository.findByLessonId(
                lessonRepository.findByShortTitleAndSection(
                        lessonName,
                        sectionRepository.findByShortTitleAndCourse(
                                sectionName,
                                courseRepository.findByShortTitle(courseName)
                                .orElseThrow(() -> new ResourceNotFoundException())
                        ).orElseThrow(() -> new ResourceNotFoundException())
                ).map(Lesson::getId).orElseThrow(() -> new ResourceNotFoundException())
        );
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons")
    public Sublesson addSublessonToCourseAndSectionAndLesson(@PathVariable String courseName,
                                                             @PathVariable String sectionName,
                                                             @PathVariable String lessonName,
                                                             @Valid @RequestBody Sublesson sublesson) {
        return lessonRepository.findByShortTitleAndSection(
                lessonName,
                sectionRepository.findByShortTitleAndCourse(
                        sectionName,
                        courseRepository.findByShortTitle(courseName).orElseThrow(() -> new ResourceNotFoundException())
                ).orElseThrow(ResourceNotFoundException::new)
        ).map(lesson -> {
            sublesson.setLesson(lesson);
            Set<Sublesson> existingSublessons = lesson.getSublessons();
            existingSublessons.add(sublesson);
            Sublesson savedSublesson = sublessonRepository.save(sublesson);
            lessonRepository.save(lesson);
            return savedSublesson;
        }).orElseThrow(ResourceNotFoundException::new);
    }

}
