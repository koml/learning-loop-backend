package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.VideoQuestionPair;
import com.koml12.tutorthingbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class VideoQuestionPairController {
    @Autowired
    private VideoQuestionPairRepository videoQuestionPairRepository;

    @Autowired
    private SublessonRepository sublessonRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;

    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons/{sublessonName}/vqpairs/")
    public List<VideoQuestionPair> getVQPairsForCourseAndSectionAndLessonAndSublesson(@PathVariable String courseName,
                                                                                      @PathVariable String sectionName,
                                                                                      @PathVariable String lessonName,
                                                                                      @PathVariable String sublessonName) {
        return videoQuestionPairRepository.findBySublesson(
                sublessonRepository.findByShortTitleAndLesson(
                        sublessonName,
                        lessonRepository.findByShortTitleAndSection(
                                lessonName,
                                sectionRepository.findByShortTitleAndCourse(
                                        sectionName,
                                        courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                                ).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        );
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons/{sublessonName}/vqpairs/")
    public VideoQuestionPair addToCourseAndSectionAndLessonAndSublesson(@PathVariable String courseName,
                                                                        @PathVariable String sectionName,
                                                                        @PathVariable String lessonName,
                                                                        @PathVariable String sublessonName,
                                                                        @Valid @RequestBody VideoQuestionPair videoQuestionPair) {
        return sublessonRepository.findByShortTitleAndLesson(
                sublessonName,
                lessonRepository.findByShortTitleAndSection(
                        lessonName,
                        sectionRepository.findByShortTitleAndCourse(
                                sectionName,
                                courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        ).map(sublesson -> {
            videoQuestionPair.setSublesson(sublesson);
            Set<VideoQuestionPair> existingVQPairs = sublesson.getVideoQuestionPairs();
            existingVQPairs.add(videoQuestionPair);
            VideoQuestionPair savedVQPair = videoQuestionPairRepository.save(videoQuestionPair);
            sublessonRepository.save(sublesson);
            return savedVQPair;
        }).orElseThrow(ResourceNotFoundException::new);
    }


}
