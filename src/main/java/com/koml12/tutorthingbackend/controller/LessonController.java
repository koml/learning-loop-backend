package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.Lesson;
import com.koml12.tutorthingbackend.model.Section;
import com.koml12.tutorthingbackend.repository.CourseRepository;
import com.koml12.tutorthingbackend.repository.LessonRepository;
import com.koml12.tutorthingbackend.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class LessonController {

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons")
    public List<Lesson> getLessonsForCourseAndSection(@PathVariable String courseName, @PathVariable String sectionName) {
        return lessonRepository.findBySectionId(
                sectionRepository.findByShortTitleAndCourse(
                        sectionName,
                        courseRepository.findByShortTitle(courseName)
                                .orElseThrow(() -> new ResourceNotFoundException("No course with name: " + courseName))
                ).map(Section::getId)
                .orElseThrow(() -> new ResourceNotFoundException("No section with name: " + sectionName + " in course: " + courseName))
        );
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons")
    public Lesson addLessonToCourseAndSection(@PathVariable String courseName,
                                              @PathVariable String sectionName,
                                              @Valid @RequestBody Lesson lesson) {
        return sectionRepository.findByShortTitleAndCourse(
                sectionName,
                courseRepository.findByShortTitle(courseName).orElseThrow(() -> new ResourceNotFoundException("No course with name: " + courseName))
        ).map(section -> {
            lesson.setSection(section);
            Set<Lesson> existingLessons = section.getLessons();
            existingLessons.add(lesson);
            Lesson savedLesson = lessonRepository.save(lesson);
            sectionRepository.save(section);
            return savedLesson;
        }).orElseThrow(() -> new ResourceNotFoundException("No section with name: " + sectionName + " in course: " + courseName));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}")
    public Lesson getLessonForLessonName(@PathVariable String courseName,
                                         @PathVariable String sectionName,
                                         @PathVariable String lessonName) {
        return lessonRepository.findByShortTitleAndSection(
                lessonName,
                sectionRepository.findByShortTitleAndCourse(
                        sectionName,
                        courseRepository.findByShortTitle(courseName)
                            .orElseThrow(() -> new ResourceNotFoundException("No course with name: " + courseName))
                ).orElseThrow(() -> new ResourceNotFoundException("No section with name: " + sectionName + " found for course: " + courseName))
                ).orElseThrow(() -> new ResourceNotFoundException("No lesson found with path: " + courseName + "/" + sectionName + "/" + lessonName));
    }
}
