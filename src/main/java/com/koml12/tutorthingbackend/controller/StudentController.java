package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.Student;
import com.koml12.tutorthingbackend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/students")
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @PostMapping("/students")
    public Student addStudent(@Valid @RequestBody Student student) {
        return studentRepository.save(student);
    }

    @GetMapping("/students/{studentId}")
    public Student getStudent(@PathVariable Long studentId) {
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("No student found with id: " + studentId));
    }

    @PutMapping("students/{studentId}")
    public Student changeStudent(@PathVariable Long studentId, @Valid @RequestBody Student newStudent) {
        return studentRepository.findById(studentId)
                .map(student -> {
                    student.replaceFields(newStudent);
                    return studentRepository.save(student);
                })
                .orElseThrow(() -> new ResourceNotFoundException("No student found with id: " + studentId));
    }
}
