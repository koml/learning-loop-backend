package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.Course;
import com.koml12.tutorthingbackend.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CourseController {

    @Autowired
    CourseRepository courseRepository;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses")
    List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/courses")
    Course addCourse(@Valid @RequestBody Course course) {
        return courseRepository.save(course);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/id/{courseId}")
    Course getCourse(@PathVariable Long courseId) {
        return courseRepository.findById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("No course found with id: " + courseId));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/courses/name/{courseShortTitle}")
    Course getCourseByShortTitle(@PathVariable String courseShortTitle) {
        return courseRepository.findByShortTitle(courseShortTitle)
                .orElseThrow(() -> new ResourceNotFoundException("No course found with short title: " + courseShortTitle));
    }
}
