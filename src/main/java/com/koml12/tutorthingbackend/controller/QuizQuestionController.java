package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.QuizQuestion;
import com.koml12.tutorthingbackend.repository.CourseRepository;
import com.koml12.tutorthingbackend.repository.LessonRepository;
import com.koml12.tutorthingbackend.repository.QuizQuestionRepository;
import com.koml12.tutorthingbackend.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class QuizQuestionController {
    @Autowired
    QuizQuestionRepository quizQuestionRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;

    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/quizquestions/")
    public List<QuizQuestion> getQuizQuestionsForCourseAndSectionAndLesson(@PathVariable String courseName,
                                                                           @PathVariable String sectionName,
                                                                           @PathVariable String lessonName) {
        return quizQuestionRepository.findByLesson(
                lessonRepository.findByShortTitleAndSection(
                        lessonName,
                        sectionRepository.findByShortTitleAndCourse(
                                sectionName,
                                courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        );
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/quizquestions/")
    public QuizQuestion addQuizQuestionToCourseAndSectionAndLesson(@PathVariable String courseName,
                                                                   @PathVariable String sectionName,
                                                                   @PathVariable String lessonName,
                                                                   @Valid @RequestBody QuizQuestion quizQuestion) {
        return lessonRepository.findByShortTitleAndSection(
                lessonName,
                sectionRepository.findByShortTitleAndCourse(
                        sectionName,
                        courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        ).map(lesson -> {
            quizQuestion.setLesson(lesson);
            Set<QuizQuestion> existingQuizQuestions = lesson.getQuizQuestions();
            existingQuizQuestions.add(quizQuestion);
            QuizQuestion savedQuizQuestion = quizQuestionRepository.save(quizQuestion);
            lessonRepository.save(lesson);
            return savedQuizQuestion;
        }).orElseThrow(ResourceNotFoundException::new);
    }


}
