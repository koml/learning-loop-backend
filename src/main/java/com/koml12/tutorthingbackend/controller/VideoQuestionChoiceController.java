package com.koml12.tutorthingbackend.controller;

import com.koml12.tutorthingbackend.exception.ResourceNotFoundException;
import com.koml12.tutorthingbackend.model.VideoQuestionChoice;
import com.koml12.tutorthingbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
public class VideoQuestionChoiceController {
    @Autowired
    private VideoQuestionChoiceRepository videoQuestionChoiceRepository;

    @Autowired
    private VideoQuestionPairRepository videoQuestionPairRepository;

    @Autowired
    private SublessonRepository sublessonRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private CourseRepository courseRepository;

    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons/{sublessonName}/vqpairs/{vqPairErrorType}/")
    public List<VideoQuestionChoice> getVQChoicesForCourseAndSectionAndLessonAndSublessonAndVQPair(@PathVariable String courseName,
                                                                                                   @PathVariable String sectionName,
                                                                                                   @PathVariable String lessonName,
                                                                                                   @PathVariable String sublessonName,
                                                                                                   @PathVariable Integer vqPairErrorType) {
        return videoQuestionChoiceRepository.findByVideoQuestionPair(
                videoQuestionPairRepository.findByErrorTypeAndSublesson(
                        vqPairErrorType,
                        sublessonRepository.findByShortTitleAndLesson(
                                sublessonName,
                                lessonRepository.findByShortTitleAndSection(
                                        lessonName,
                                        sectionRepository.findByShortTitleAndCourse(
                                                sectionName,
                                                courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                                        ).orElseThrow(ResourceNotFoundException::new)
                                ).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        );
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @PostMapping("/courses/name/{courseName}/sections/name/{sectionName}/lessons/name/{lessonName}/sublessons/{sublessonName}/vqpairs/{vqPairErrorType}/")
    public VideoQuestionChoice addVQChoiceToVQPairFromCourseAndSectionAndLessonAndSublesson(@PathVariable String courseName,
                                                                                            @PathVariable String sectionName,
                                                                                            @PathVariable String lessonName,
                                                                                            @PathVariable String sublessonName,
                                                                                            @PathVariable Integer vqPairErrorType,
                                                                                            @Valid @RequestBody VideoQuestionChoice videoQuestionChoice) {
        return videoQuestionPairRepository.findByErrorTypeAndSublesson(
                vqPairErrorType,
                sublessonRepository.findByShortTitleAndLesson(
                        sublessonName,
                        lessonRepository.findByShortTitleAndSection(
                                lessonName,
                                sectionRepository.findByShortTitleAndCourse(
                                        sectionName,
                                        courseRepository.findByShortTitle(courseName).orElseThrow(ResourceNotFoundException::new)
                                ).orElseThrow(ResourceNotFoundException::new)
                        ).orElseThrow(ResourceNotFoundException::new)
                ).orElseThrow(ResourceNotFoundException::new)
        ).map(videoQuestionPair -> {
            videoQuestionChoice.setVideoQuestionPair(videoQuestionPair);
            Set<VideoQuestionChoice> existingVideoQuestionChoices = videoQuestionPair.getVideoQuestionChoices();
            existingVideoQuestionChoices.add(videoQuestionChoice);
            VideoQuestionChoice savedVQChoice = videoQuestionChoiceRepository.save(videoQuestionChoice);
            videoQuestionPairRepository.save(videoQuestionPair);
            return savedVQChoice;
        }).orElseThrow(ResourceNotFoundException::new);
    }

}
