package com.koml12.tutorthingbackend.client;

import com.google.gson.JsonObject;

public class CourseClient {
    /**
     * Gets a complete set of lesson data for the given short titles.
     *
     * @param course    shortTitle for the course.
     * @param section   shortTitle for the section.
     * @param lesson    shortTitle for the lesson.
     * @return          complete set of JSON data for the lesson.
     */
    public JsonObject getCourseData(String course, String section, String lesson) {
        return null;
    }
}
