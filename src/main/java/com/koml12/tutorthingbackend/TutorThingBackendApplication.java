package com.koml12.tutorthingbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorThingBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TutorThingBackendApplication.class, args);
    }

}
