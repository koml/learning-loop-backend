package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Lesson;
import com.koml12.tutorthingbackend.model.QuizQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface QuizQuestionRepository extends JpaRepository<QuizQuestion, Long> {
    List<QuizQuestion> findByLesson(Lesson lesson);

    Optional<QuizQuestion> findByQuestionNumberAndLesson(Integer questionNumber, Lesson lesson);
}
