package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Lesson;
import com.koml12.tutorthingbackend.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findBySectionId(Long sectionId);

    Optional<Lesson> findByShortTitleAndSection(String shortTitle, Section section);


}
