package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    Optional<Course> findByShortTitle(String shortTitle);


}
