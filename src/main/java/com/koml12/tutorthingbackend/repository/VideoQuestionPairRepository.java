package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Sublesson;
import com.koml12.tutorthingbackend.model.VideoQuestionPair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VideoQuestionPairRepository extends JpaRepository<VideoQuestionPair, Long> {
    List<VideoQuestionPair> findBySublesson(Sublesson sublesson);

    Optional<VideoQuestionPair> findByErrorTypeAndSublesson(Integer errorType, Sublesson sublesson);
}
