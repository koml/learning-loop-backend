package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.QuizChoice;
import com.koml12.tutorthingbackend.model.QuizQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuizChoiceRepository extends JpaRepository<QuizChoice, Long> {
    List<QuizChoice> findByQuizQuestion(QuizQuestion quizQuestion);
}
