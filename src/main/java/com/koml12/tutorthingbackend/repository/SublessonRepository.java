package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Lesson;
import com.koml12.tutorthingbackend.model.Sublesson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SublessonRepository extends JpaRepository<Sublesson, Long> {
    Optional<Sublesson> findByShortTitleAndLesson(String shortTitle, Lesson lesson);

    List<Sublesson> findByLessonId(Long lessonId);
}
