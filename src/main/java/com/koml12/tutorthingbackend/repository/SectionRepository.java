package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.Course;
import com.koml12.tutorthingbackend.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {
    List<Section> findByCourseId(Long courseId);

    Optional<Section> findByShortTitleAndCourse(String shortTitle, Course course);
}
