package com.koml12.tutorthingbackend.repository;

import com.koml12.tutorthingbackend.model.VideoQuestionChoice;
import com.koml12.tutorthingbackend.model.VideoQuestionPair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoQuestionChoiceRepository extends JpaRepository<VideoQuestionChoice, Long> {
    List<VideoQuestionChoice> findByVideoQuestionPair(VideoQuestionPair videoQuestionPair);

}
